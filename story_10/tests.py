from .apps import Story10Config
from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story10Config.name, 'story_10')
		self.assertEqual(apps.get_app_config('story_10').name, 'story_10')

class Story10UnitTest(TestCase):
	def test_homepage_url_exists(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_signup_url_exists(self):
		response = Client().get('/signup/')
		self.assertEqual(response.status_code, 200)

	def test_login_url_exists(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_logout_url_exists(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_update_profile_url_exists(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 200)

	def test_create_model(self):
		User.objects.create_user(username="ha", email="ha@gmail.com", password="haha")
		self.assertEqual(User.objects.all().count(), 1)

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()

	def test_session(self):
		self.browser.get(self.live_server_url)
		time.sleep(3)

		# Click the login button
		login = self.browser.find_element_by_name('login')
		login.click()

		# Try to log in
		username = self.browser.find_element_by_name('username')
		username.send_keys('abc')
		password = self.browser.find_element_by_name('password')
		password.send_keys('hajakala')
		time.sleep(3)
		password.send_keys(Keys.RETURN)
		time.sleep(3)

		# It won't success since the account hasn't existed yet
		alert = self.browser.switch_to.alert
		time.sleep(3)
		alert.accept()
		
		# Let's go back to home, and now click sign up
		self.browser.get(self.live_server_url)
		time.sleep(3)
		signup = self.browser.find_element_by_name('signup')
		signup.click()

		# Now create a new account
		username = self.browser.find_element_by_name('username')
		email = self.browser.find_element_by_name('email')
		password = self.browser.find_element_by_name('password')
		
		username.send_keys('abc')
		email.send_keys('abc@gmail.com')
		password.send_keys('hajakala')
		time.sleep(3)
		password.send_keys(Keys.RETURN)
		time.sleep(3)
		
		# After that, try to login
		username = self.browser.find_element_by_name('username')
		password = self.browser.find_element_by_name('password')
		username.send_keys('abc')
		password.send_keys('ha')
		time.sleep(3)
		password.send_keys(Keys.RETURN)
		time.sleep(3)

		# It won't success since the password is wrong
		alert = self.browser.switch_to.alert
		time.sleep(3)
		alert.accept()
		
		# Now try to login with the correct password
		username = self.browser.find_element_by_name('username')
		password = self.browser.find_element_by_name('password')
		username.send_keys('abc')
		password.send_keys('hajakala')
		time.sleep(3)
		password.send_keys(Keys.RETURN)
		time.sleep(3)

		# Since we've logged in, our username will be displayed on homepage
		self.assertIn('abc', self.browser.page_source)

		# Now let's update our profile
		profile = self.browser.find_element_by_name('profile')
		profile.click()

		# Change bio and profile picture
		bio = self.browser.find_element_by_name('bio')
		image = self.browser.find_element_by_name('image')
		bio.send_keys('ini bio saya')
		image.send_keys('https://vectorified.com/images/avatar-icon-png-9.jpg')
		time.sleep(3)
		image.send_keys(Keys.RETURN)
		time.sleep(3)

		# Now our bio and profile picture are displayed on homepage
		self.assertIn('ini bio saya', self.browser.page_source)

		# Now try to log out
		logout = self.browser.find_element_by_name('logout')
		logout.click()

		# Anyway it's not possible to sign up for an axisting username
		signup = self.browser.find_element_by_name('signup')
		signup.click()
		username = self.browser.find_element_by_name('username')
		email = self.browser.find_element_by_name('email')
		password = self.browser.find_element_by_name('password')
		
		username.send_keys('abc')
		email.send_keys('aaa@gmail.com')
		password.send_keys('hajakala')
		password.send_keys(Keys.RETURN)
		time.sleep(3)

		alert = self.browser.switch_to.alert
		time.sleep(3)
		alert.accept()
