from django.db import models

# Create your models here.
class Book(models.Model):
	id = models.CharField('Id', max_length=200, primary_key=True)
	cover = models.CharField('Cover', max_length=200)
	title = models.CharField('Title', max_length=200)
	author = models.CharField('Author', max_length=200)
	publisher = models.CharField('Publisher', max_length=200)
	publishedDate = models.CharField('Published Date', max_length=200)
	likes = models.PositiveIntegerField('Likes')