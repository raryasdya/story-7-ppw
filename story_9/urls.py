from django.urls import path
from . import views

app_name = 'story_9'

urlpatterns = [
    path('', views.books, name='books'),
    path('create-book/', views.create_book, name='create_book'),
    path('add-like/', views.add_like, name='add_like'),
    path('top/', views.top, name='top'),
]