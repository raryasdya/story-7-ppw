from django.forms.models import model_to_dict
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseForbidden
from .models import Book

# Create your views here.
def books(request) :
	return render(request, 'books.html')

@csrf_exempt
def create_book(request) :
	if(request.is_ajax()) :
		data = {}
		if(Book.objects.filter(id=request.POST['id']).exists()) :
			existing_book = Book.objects.get(id=request.POST['id'])
			data['likes'] = existing_book.likes
		else :
			data['likes'] = 0
			Book.objects.create(
	            id=request.POST['id'],
	            cover=request.POST['cover'],
	            title=request.POST['title'],
	            author=request.POST['author'],
	            publisher=request.POST['publisher'],
	            publishedDate=request.POST['publishedDate'],
	            likes=0
	        )
		return JsonResponse(data)
	else :
		return HttpResponseForbidden()

@csrf_exempt
def add_like(request) :
	if(request.is_ajax()) :
		book = Book.objects.get(id=request.POST['id'])
		book.likes += 1
		book.save()
		data = {'likes' : book.likes}
		return JsonResponse(data)
	else :
		return HttpResponseForbidden()

def top(request) :
	if(request.is_ajax()) :
		top5 = [model_to_dict(book) for book in Book.objects.order_by('-likes')[:5]]
		return JsonResponse({'items':top5})
	else :
		return HttpResponseForbidden()