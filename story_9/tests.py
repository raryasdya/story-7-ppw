from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .apps import Story9Config
from .models import Book
import time

# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story9Config.name, 'story_9')
		self.assertEqual(apps.get_app_config('story_9').name, 'story_9')

class Story9UnitTest(TestCase):
	def test_book_url_exists(self):
		response = Client().get('/books/')
		self.assertEqual(response.status_code, 200)

	def test_create_book_url_exists(self):
		response = Client().get('/books/create-book/')
		self.assertEqual(response.status_code, 403)

	def test_add_like_url_exists(self):
		response = Client().get('/books/add-like/')
		self.assertEqual(response.status_code, 403)

	def test_top_url_exists(self):
		response = Client().get('/books/top/')
		self.assertEqual(response.status_code, 403)

	def test_status_model_create_new_object(self):
		book = Book(
			id='abc',
			cover='abc',
			title='abc',
			author='abc',
			publisher='abc',
			publishedDate=2010,
			likes=0
		)
		book.save()
		self.assertEqual(Book.objects.all().count(), 1)

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()

	def test_search(self):
		self.browser.get(self.live_server_url + '/books')

		# Check the page's title
		self.assertIn('BOOKS', self.browser.page_source)
	
		# Let's search book(s)
		search = self.browser.find_element_by_id('search')
		search.send_keys('Ulysses Moore')
		time.sleep(5)

		# Since we've typed some words to the search box
		# The list will show temporary result
		temp_list = self.browser.find_element_by_tag_name('ul').text
		self.assertIn('Ulysses Moore', temp_list)

		# Press enter so the result table will show
		search.send_keys(Keys.RETURN)
		time.sleep(5)

		# Get the title for the first book
		first_book = self.browser.find_element_by_xpath('//table[@id="result"]/tr[1]/td[2]').text
		like_first_book = self.browser.find_element_by_xpath('//table[@id="result"]/tr[1]/td[6]/div').text

		# Press the like button 3 times
		like_button = self.browser.find_element_by_xpath('//table[@id="result"]/tr[1]/td[6]/button')
		like_button.click()
		time.sleep(5)
		like_button.click()
		time.sleep(5)
		like_button.click()
		time.sleep(5)
		
		# Originally it has got zero like
		# Now, it has got 3 likes
		new_like_first_book = self.browser.find_element_by_xpath('//table[@id="result"]/tr[1]/td[6]/div').text
		self.assertEqual(int(like_first_book)+3, int(new_like_first_book))

		# Now, if we search another book, the result will change
		search = self.browser.find_element_by_id('search')
		search.clear()
		search.send_keys('Harry Potter')
		time.sleep(5)

		# Since we've typed some new words to the search box
		# The list will show temporary result that has changed
		temp_list = self.browser.find_element_by_tag_name('ul').text
		self.assertIn('Harry Potter', temp_list)
		self.assertNotIn('Ulysses Moore', temp_list)

		# Press enter so the result table will show
		search.send_keys(Keys.RETURN)
		time.sleep(5)

		# The results have changed
		result = self.browser.find_element_by_id('result').text
		self.assertIn('Harry Potter', result)
		self.assertNotIn('Ulysses Moore', result)

		# Get the title for the first book
		second_book = self.browser.find_element_by_xpath('//table[@id="result"]/tr[1]/td[2]').text
		like_second_book = self.browser.find_element_by_xpath('//table[@id="result"]/tr[1]/td[6]/div').text

		# Press the like button 2 times
		like_button = self.browser.find_element_by_xpath('//table[@id="result"]/tr[1]/td[6]/button')
		like_button.click()
		time.sleep(5)
		like_button.click()
		time.sleep(5)
		
		# Originally it has got zero like
		# Now, it has got 2 likes
		new_like_second_book = self.browser.find_element_by_xpath('//table[@id="result"]/tr[1]/td[6]/div').text
		self.assertEqual(int(like_second_book)+2, int(new_like_second_book))

		# Anyway, if we search the previous book
		search = self.browser.find_element_by_id('search')
		search.clear()
		search.send_keys('Ulysses Moore')
		time.sleep(5)
		search.send_keys(Keys.RETURN)
		time.sleep(5)

		# The amount of likes won't change
		result = self.browser.find_element_by_tag_name('table').text
		self.assertIn(new_like_first_book, result)

		# This time, let's check the top 5 book's modal
		modal_button = self.browser.find_element_by_id('top')
		modal_button.click()
		time.sleep(5)

		# Since the first booh has 3 likes and the second book has 2 likes
		# The first book must show up before the second book
		top5 = self.browser.find_element_by_id('top-table').text
		self.assertTrue(top5.find(first_book) < top5.find(second_book))