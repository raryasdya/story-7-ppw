from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from .views import status, confirm
from .apps import Story7Config
from .models import Status
from .forms import StatusForm
import time

# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story7Config.name, 'story_7')
		self.assertEqual(apps.get_app_config('story_7').name, 'story_7')

class Story7UnitTest(TestCase):
	def test_homepage_url_exists(self):
		response = Client().get('/status/')
		self.assertEqual(response.status_code, 200)

	def test_confirmation_url_exists(self):
		response = Client().post('/status/confirm/', {'nama': "abc", 'pesan': "123"})
		self.assertEqual(response.status_code, 200)

	def test_change_color_url_exists_temporary(self):
		status = Status(nama="abc", pesan="ini pesan", warna="#008081")
		status.save()
		response = Client().post('/status/1/')
		self.assertEqual(response.status_code, 302)

	def test_status_model_create_new_object(self):
		status = Status(nama="abc", pesan="ini pesan", warna="#008081")
		status.save()
		self.assertEqual(Status.objects.all().count(), 1)

	def test_post_success(self):
		response = Client().post('/status/', {'nama': "abc", 'pesan': "123"})
		html_response = response.content.decode('utf8')
		self.assertIn('abc', html_response)
		self.assertIn('123', html_response)

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()

	def test_confirmed_input_form(self):
		self.browser.get(self.live_server_url + '/status/')
		
		name = self.browser.find_element_by_name('nama')
		name.send_keys('abc')
		
		message = self.browser.find_element_by_name('pesan')
		message.send_keys('ini pesannya')

		time.sleep(5)

		submit = self.browser.find_element_by_id('submit')
		submit.click()

		time.sleep(5)

		self.assertIn('Are you sure', self.browser.page_source)
		yes = self.browser.find_element_by_id('yes')
		yes.click()

		self.assertIn('abc', self.browser.page_source)
		self.assertIn('ini pesannya', self.browser.page_source)

	def test_cancelled_input_form(self):
		self.browser.get(self.live_server_url + '/status/')
		
		name = self.browser.find_element_by_name('nama')
		name.send_keys('abc')
		
		message = self.browser.find_element_by_name('pesan')
		message.send_keys('ini pesannya')

		time.sleep(5)

		submit = self.browser.find_element_by_id('submit')
		submit.click()

		time.sleep(5)

		self.assertIn('Are you sure', self.browser.page_source)
		no = self.browser.find_element_by_id('no')
		no.click()

		self.assertNotIn('abc', self.browser.page_source)
		self.assertNotIn('ini pesannya', self.browser.page_source)

	def test_change_color(self):
		status = Status(nama="abc", pesan="ini pesan", warna="#008081")
		status.save()

		self.browser.get(self.live_server_url + '/status/')
		old_color = self.browser.find_element_by_class_name('card').value_of_css_property('background-color')

		change = self.browser.find_element_by_name('change')
		change.click()

		new_color = self.browser.find_element_by_class_name('card').value_of_css_property('background-color')
		
		self.assertNotEqual(old_color, new_color)