from django.urls import path
from . import views

app_name = 'story_7'

urlpatterns = [
    path('', views.status, name='status'),
    path('confirm/', views.confirm, name='confirm'),
    path('<int:index>/', views.change, name='change'),
]