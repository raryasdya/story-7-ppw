from django.shortcuts import render, redirect
import random
from .models import Status
from .forms import StatusForm

# Create your views here.
def status(request):
	if request.method == 'POST':
		nama = request.POST['nama']
		pesan = request.POST['pesan']
		red = str(hex(random.randint(0, 255)))[2:]
		green = str(hex(random.randint(0, 255)))[2:]
		blue = str(hex(random.randint(0, 255)))[2:]
		warna = '#' + red + green + blue
		Status.objects.create(nama=nama, pesan=pesan, warna=warna)

	status_list = Status.objects.all()
	form = StatusForm()

	context = {
		'status': status_list,
		'form' : form
	}
	return render(request, 'status.html', context=context)

def change(request, index):
	status = Status.objects.get(pk=index)
	warna = status.warna

	while status.warna == warna :
		red = str(hex(random.randint(0, 255)))[2:]
		green = str(hex(random.randint(0, 255)))[2:]
		blue = str(hex(random.randint(0, 255)))[2:]
		warna = '#' + red + green + blue

	status.warna = warna
	status.save()

	return redirect('/status/')

def confirm(request):
	if request.method == 'POST':
		form = StatusForm(request.POST)
		context = {
			'nama' : form.data['nama'],
	        'pesan' : form.data['pesan']
	        }
		return render(request, 'confirm.html', context=context)