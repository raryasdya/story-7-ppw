from django.db import models

# Create your models here.
class Status(models.Model):
	nama = models.CharField('Name', max_length=20)
	pesan = models.TextField('Message', max_length=280)
	warna = models.CharField('Color', max_length=8, null=True)