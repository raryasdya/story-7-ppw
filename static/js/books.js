$(document).ready(function() {
	$.ajaxSetup({ cache: false });

	$('#search').keyup(function(event){
		$('#list_result').html('');
		$('#result').html('');
		var searchField = $('#search').val();

		if (event.keyCode != 13) {
		  $.get("https://www.googleapis.com/books/v1/volumes?q="+searchField, function(data){
				$.each(data.items, function(key, value){
					var x = value.volumeInfo;
	    		$('#list_result').append('<li class="list-group-item">'+x.title+'</li>');
		  	});
			});
		} else {
			console.log("search " + searchField);

			$.get("https://www.googleapis.com/books/v1/volumes?q="+searchField, function(data){
				$('#result').append(`
					<thead class="align-middle text-center font-weight-bold">
						<tr>
							<th> Cover </th>
							<th> Title </th>
							<th> Author </th>
							<th> Publisher </th>
							<th> Published Date </th>
							<th> Likes </th>
						</tr>
					</thead>
				`);
				
				$.each(data.items, function(key, value){
					var x = value.volumeInfo;
					var book = {
						id: value.id,
						cover: x.imageLinks.smallThumbnail,
						title: x.title,
						author: x.authors[0],
						publisher: x.publisher,
						publishedDate: x.publishedDate
					}

					$.post('/books/create-book/', book, function(data){
						$('#result').append(`
							<tr>
								<td><img src=` + book.cover + ` width="120px"></td>
								<td>` + book.title + `</td>
								<td>` + book.author + `</td>
								<td>` + book.publisher + `</td>
								<td>` + book.publishedDate + `</td>
								<td class="text-center"><div class="` + book.id + `">` + data.likes + `</div>
								<br><button id="` + book.id + `" class="like"> Like! </button></td>
							</tr>
						`);
					});
				});
			});
		}
	});

	$(document).on("click", ".like", function () {
		book_id = this.id
		console.log("add like to " + book_id);
		$.post('/books/add-like/', {id:book_id}, function(data){
	  	$('.' + book_id).html('');
			$('.' + book_id).append(data.likes);
		});
	});

	$(document).on("click", "#top", function () {
		console.log('top 5');
		$('#top-table').html('');
		$.get('/books/top/', function(data){
			$.each(data.items, function(key, value){
		  	$('#top-table').append(`
		  		<tr>
						<td><img src=` + value.cover + ` width="120px"></td>
						<td>` + value.title + `</td>
						<td>` + value.author + `</td>
						<td class="text-center">` + value.likes + `</div>
					</tr>
		  	`);
		  });
		});
	});
});