from django.urls import path
from . import views

app_name = 'story_8'

urlpatterns = [
    path('', views.about, name='about'),
]