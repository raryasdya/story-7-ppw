from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from .apps import Story8Config
from .views import about
import time

# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story8Config.name, 'story_8')
		self.assertEqual(apps.get_app_config('story_8').name, 'story_8')

class Story8UnitTest(TestCase):
	def test_about_url_exists(self):
		response = Client().get('/about/')
		self.assertEqual(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()

	def test_accordion(self):
		self.browser.get(self.live_server_url + '/about')
		
		# Check the page's title
		self.assertIn('ABOUT ME', self.browser.page_source)

		# Since every content is still hidden,
		# then there will be no information about current activity
		text = self.browser.find_element_by_tag_name('body').text
		self.assertNotIn('assignments', text)

		# Press the first bar (Current Activity) to open the content
		bar = self.browser.find_element_by_id('activity')
		self.assertIn('Current Activity', bar.text)
		bar.click()
		time.sleep(1)

		# The first content is not hidden anymore,
		# then there will be an information about current activity
		text = self.browser.find_element_by_tag_name('body').text
		self.assertIn('assignments', text)

		# Only the Current Activity content is opened,
		# then there will be no information about experience
		self.assertNotIn('Relation Division', text)

		# Press the second bar (Experience) to open the content
		bar = self.browser.find_element_by_id('experience')
		self.assertIn('Experience', bar.text)
		bar.click()
		time.sleep(1)

		# The second content is not hidden anymore,
		# then there will be an information about experience
		text = self.browser.find_element_by_tag_name('body').text
		self.assertIn('Relation Division', text)

		# Only the Experience content is opened,
		# then there will be no information about Current Activity
		self.assertNotIn('assignments', text)

		# And there will be no information either about Achievement
		self.assertNotIn('achieved', text)

		# Press the third bar (Achievement) to open the content
		bar = self.browser.find_element_by_id('achievement')
		self.assertIn('Achievement', bar.text)
		bar.click()
		time.sleep(1)

		# The third content is not hidden anymore,
		# then there will be an information about Achievement
		text = self.browser.find_element_by_tag_name('body').text
		self.assertIn('achieved', text)

		# Only the Achievement content is opened,
		# then there will be no information about Experience
		self.assertNotIn('Relation Division', text)

		# And there will be no information either about Contact
		self.assertNotIn('fairuza.raryasdya@ui.ac.id', text)

		# Press the last bar (Contact) to open the content
		bar = self.browser.find_element_by_id('contact')
		self.assertIn('Contact', bar.text)
		bar.click()
		time.sleep(1)

		# The last content is not hidden anymore,
		# then there will be an information about Contact
		text = self.browser.find_element_by_tag_name('body').text
		self.assertIn('fairuza.raryasdya@ui.ac.id', text)

		# Only the Contact content is opened,
		# then there will be no information about Achievement
		self.assertNotIn('achieved', text)

		# Now that we've known that every accordion works properly,
		# it's time to check the up and down feature

		# Move the last bar (Contact) to the 3rd position
		up = self.browser.find_element_by_xpath("//div[@id='contact']//button[@class='up']")
		up.click()
		time.sleep(1)

		# Now Contact is above Achievement
		text = self.browser.find_element_by_tag_name('body').text
		self.assertTrue(text.find('Contact') < text.find('Achievement'))

		# Let's try to move the last bar (Achievement) to the top
		up = self.browser.find_element_by_xpath("//div[@id='achievement']//button[@class='up']")
		up.click()
		up.click()
		up.click()
		time.sleep(1)

		# Now Achievement is at the top
		text = self.browser.find_element_by_tag_name('body').text
		self.assertTrue(text.find('Achievement') < text.find('Activity'))
		self.assertTrue(text.find('Achievement') < text.find('Experience'))
		self.assertTrue(text.find('Achievement') < text.find('Contact'))

		# Let's return it to the original position
		down = self.browser.find_element_by_xpath("//div[@id='achievement']//button[@class='down']")
		down.click()
		down.click()
		time.sleep(1)

		# Now everything is back to normal
		text = self.browser.find_element_by_tag_name('body').text
		self.assertTrue(text.find('Activity') < text.find('Experience'))
		self.assertTrue(text.find('Experience') < text.find('Achievement'))
		self.assertTrue(text.find('Achievement') < text.find('Contact'))

	def test_change_theme(self):
		self.browser.get(self.live_server_url + '/about')

		# We want to change the color theme
		# So first, we list the original color first
		old_bg = self.browser.find_element_by_tag_name('section').value_of_css_property('background-color')
		old_title = self.browser.find_element_by_class_name('section-title').value_of_css_property('color')
		old_button_up = self.browser.find_element_by_class_name('up').value_of_css_property('background-color')
		old_button_down = self.browser.find_element_by_class_name('down').value_of_css_property('background-color')
		old_bar = self.browser.find_element_by_class_name('bar').value_of_css_property('background-color')
		old_content = self.browser.find_element_by_class_name('content').value_of_css_property('background-color')

		# Now press the button so the theme will change
		button = self.browser.find_element_by_class_name('theme')
		self.assertIn('Change Theme', button.text)
		button.click()

		# The color theme has changed!
		# So now, we list the new color
		new_bg = self.browser.find_element_by_tag_name('section').value_of_css_property('background-color')
		new_title = self.browser.find_element_by_class_name('section-title').value_of_css_property('color')
		new_button_up = self.browser.find_element_by_class_name('up').value_of_css_property('background-color')
		new_button_down = self.browser.find_element_by_class_name('down').value_of_css_property('background-color')
		new_bar = self.browser.find_element_by_class_name('bar').value_of_css_property('background-color')
		new_content = self.browser.find_element_by_class_name('content').value_of_css_property('background-color')

		# And lastly, we compare the previous color and the new one
		# Because we changed the theme, the old and new color shouldn't be same
		self.assertNotEqual(old_bg, new_bg)
		self.assertNotEqual(old_title, new_title)
		self.assertNotEqual(old_button_up, new_button_up)
		self.assertNotEqual(old_button_down, new_button_down)
		self.assertNotEqual(old_bar, new_bar)
		self.assertNotEqual(old_content, new_content)